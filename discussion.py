# username = input("Please enter your name:\n")

# print(f"Hello {username} welcome to python short course")

# num1 = int(input("Enter first number: "))
# num2 = int(input("Enter second number: "))

# print(f"The sum of num1 and num2 is {num1 + num2}")


# f-string

# typecasting

# [section] with user inputs, users can give inputs for the program to be used to control the application using contol structures.



# test_num = 75

# if test_num >= 60 :
# 	print("Test passed!")
# else:
# 	print("Test failed!")


num: int = int(input("Enter numer: "))
if num % 3 == 0 and num % 5 == 0:
	print(f"{num} is divisible by 3 and 5")
elif num % 3  == 0:
	print(f"{num} is divisible by 3")
elif num % 5 == 0:
	print(f"{num} is divisible by 5")
else:
	print(f"{num} is not divisible by 3 nor 5")